import React from 'react';
import ReactDOM from 'react-dom';

import { Main } from "./pages";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'


import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'antd/dist/antd.min.css';

const routing = (
        <Router>
            <Switch>
                <Route exact path={ '/' } component={ Main }/>
            </Switch>
        </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
