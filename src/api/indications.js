import request from '../utils/request'

export function get() {
    return request({
        url: '/indication',
        method: 'get'
    })
}

