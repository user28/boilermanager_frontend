import React from 'react'
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    AreaChart, Area
} from 'recharts';

import './style.css'
import { get } from "../../api/indications";
import { Col, Descriptions, Row, Switch } from "antd";
import moment from "moment";

const dateFormat = "D.MM.YYYY H:mm:ss";

const CustomTooltip = ({active, payload, label}) => {
    if (active) {
        const date = moment(label).format(dateFormat);
        const data = payload[0].payload;

        const {outlet_temperature, room_temperature, room_thermostat_setting, room_thermostat, power} = data;

        return (
            <div className="chart-tooltip">
                <p className="label">{ `Дата: ${ date }` }</p>
                <span className="data-row">{ `Температура в комнате: ${ room_temperature }` }</span>
                <span className="data-row">{ `Температура на выходе: ${ outlet_temperature }` }</span>
                <span className="data-row">{ `Установка термостата: ${ room_thermostat_setting }` }</span>
                <span className="data-row">{ `Нагрев: ${ room_thermostat ? 'Включен' : 'Выключен' }` }</span>
                <span className="data-row">{ `Питание: ${ power ? 'Включено' : 'Выключено' }` }</span>
            </div>
        );
    }

    return null;
};

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            indications: [],
            current_power: true,
            current_room_thermostat: true,
        };

        get().then(response => {
            const {indications} = response;
            this.setState({
                indications: indications,
                current_power: indications[indications.length - 1].power,
                current_room_thermostat: indications[indications.length - 1].room_thermostat,
                loading: false
            })
        })
    };

    formatXAxis = (item) => {
        return moment(item).format(dateFormat)
    };

    legendFormatter = (value, entry, index) => {
        switch (value) {
            case 'outlet_temperature':
                return 'Температура на выходе';
            case 'room_temperature':
                return 'Температура в комнате';
            default:
                return ''
        }
    };

    handleChangePower = (event) => {
        this.setState({
            current_power: event
        })
    };

    handleChangeRoomThermostat = (event) => {
        this.setState({
            current_room_thermostat: event
        })
    };

    render() {
        const {indications, loading, current_power, current_room_thermostat} = this.state;
        const last = indications[indications.length - 1];

        if (loading) {
            return (
                <div>
                    Loading...
                </div>
            )
        }

        const {
            outlet_temperature,
            room_temperature,
            water_pressure,
            gas_ch4,
            gas_co,
            power,
            room_thermostat,
            room_thermostat_setting
        } = last;

        let water_pressure_label = '';

        switch (water_pressure) {
            case 0:
                water_pressure_label = 'Низкое';
                break;
            case 1:
                water_pressure_label = 'В норме';
                break;
            case 2:
                water_pressure_label = 'Высокое';
                break;
            default:
                water_pressure_label = 'Данные не получены';
                break;
        }

        return (
            <div>
                <Row>
                    <Col span={ 24 }>
                        <ResponsiveContainer width="100%" height={ 400 }>
                            <AreaChart data={ indications }
                                       margin={ {top: 30, right: 30, left: 20, bottom: 5} }
                                       padding={ {top: 30} }
                                       syncId={1}
                            >
                                <CartesianGrid strokeDasharray="3 3"/>
                                <XAxis dataKey="created_at" tickFormatter={ this.formatXAxis }/>
                                <YAxis/>
                                <Tooltip content={ <CustomTooltip/> }/>
                                <Legend formatter={ this.legendFormatter }/>
                                <Area type="monotone" dataKey="outlet_temperature" stroke="#1bca71" fill="#1bca71"/>
                            </AreaChart>
                        </ResponsiveContainer>
                        <ResponsiveContainer width="100%" height={ 400 }>
                            <AreaChart data={ indications }
                                       margin={ {top: 30, right: 30, left: 20, bottom: 5} }
                                       padding={ {top: 30} }
                                       syncId={1}
                            >
                                <CartesianGrid strokeDasharray="3 3"/>
                                <XAxis dataKey="created_at" tickFormatter={ this.formatXAxis }/>
                                <YAxis/>
                                <Tooltip content={ <CustomTooltip/> }/>
                                <Legend formatter={ this.legendFormatter }/>
                                <Area type="monotone" dataKey="room_temperature" stroke="#d87869" fill="#d87869"/>
                            </AreaChart>
                        </ResponsiveContainer>
                    </Col>
                </Row>
                <Row className="last-info">
                    <Col span={ 22 } offset={ 2 }>
                        <Descriptions title="Последняя информация">
                            <Descriptions.Item
                                label="Температура на выходе">{ outlet_temperature } ℃</Descriptions.Item>
                            <Descriptions.Item label="Давление">{ water_pressure_label }</Descriptions.Item>
                            <Descriptions.Item
                                label="Загозованость по CO">{ `${ gas_co ? 'Присутствует загазованость' : 'Загазованость отсутствует' }` }</Descriptions.Item>
                            <Descriptions.Item label="Температура в комнате">{ room_temperature } ℃</Descriptions.Item>
                            <Descriptions.Item label="Питание"><Switch checked={ current_power }
                                                                       onChange={ this.handleChangePower }/></Descriptions.Item>
                            <Descriptions.Item
                                label="Загозованость по CH">{ `${ gas_ch4 ? 'Присутствует загазованость' : 'Загазованость отсутствует' }` }</Descriptions.Item>
                            <Descriptions.Item
                                label="Установка термостата">{ room_thermostat_setting } ℃</Descriptions.Item>
                            <Descriptions.Item label="Нагрев"><Switch checked={ current_room_thermostat }
                                                                      onChange={ this.handleChangeRoomThermostat }/></Descriptions.Item>
                        </Descriptions>
                    </Col>
                </Row>
            </div>
        );
    }
}
