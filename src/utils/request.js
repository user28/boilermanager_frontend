import axios from 'axios';

const service = axios.create({
    baseURL: process.env.REACT_APP_BACKEND_API,
    timeout: 5000
});

service.interceptors.request.use(
    config => {
        return config;
    },
    error => {
        console.log(error);
        return Promise.reject(error);
    }
);

service.interceptors.response.use(
    response => {
        const res = response.data;

        if (res.success !== true) {
            return Promise.reject(res);
        } else {
            return res;
        }
    },
    error => {
        if (error.response && 401 === error.response.status) {
            localStorage.clear();
            window.location = '/auth';
        }
        return Promise.reject(error);
    }
);

export default service
